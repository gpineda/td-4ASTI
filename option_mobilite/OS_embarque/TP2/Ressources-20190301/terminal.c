/** ====================================================================
**   Auteur  : FERRERE                | Date    : 27/01/2013
**  --------------------------------------------------------------------
**   Langage : C                      | Systeme : Linux 2.6
**  --------------------------------------------------------------------
**   Nom fichier : terminal.c         | Version : 1.0
**  --------------------------------------------------------------------
**   Description : librairie de fonction d'un terminal
** =====================================================================*/

#include <stdio.h>
#include <termios.h>
#include "terminal.h"

/*======================================================================
**  Nom          : _raw
**  Description  : configure le clavier dans le mode raw
** ---------------------------------------------------------------------
**  ValRetournée : -1 si mauvais descripteur de fichier
** ---------------------------------------------------------------------
**  Parametres   : [E] [S] [ES]
**      1 [E]    : descripteur de fichier
**======================================================================*/
int _raw (int fd)
{
	struct termios termios_p;

	if ( tcgetattr( fd, &termios_p ) )
		return( -1 );

	termios_p.c_cc[VMIN]  =  0;
	termios_p.c_cc[VTIME] =  0;
	termios_p.c_lflag &= ~( ECHO|ICANON|ISIG|ECHOE|ECHOK|ECHONL );
	termios_p.c_oflag &= ~( OPOST );
	return( tcsetattr( fd, TCSADRAIN, &termios_p ) );
}

/*======================================================================
**  Nom          : _unraw
**  Description  : configure le clavier dans le mode edited
** ---------------------------------------------------------------------
**  ValRetournée : -1 si mauvais descripteur de fichier
** ---------------------------------------------------------------------
**  Parametres   : [E] [S] [ES]
**      1 [E]    : descripteur de fichier
**======================================================================*/
int _unraw (int fd)
{
	struct termios termios_p;

	if ( tcgetattr( fd, &termios_p ) )
		return( -1 );

	termios_p.c_lflag |= ( ECHO|ICANON|ISIG|ECHOE|ECHOK|ECHONL );
	termios_p.c_oflag |= ( OPOST );
	return( tcsetattr( fd, TCSADRAIN, &termios_p ) );
}

/*======================================================================
**  Nom          : _clrscr
**  Description  : efface l'écran du terminal
**                 place le curseur au début de l'écran
**======================================================================*/
void _clrscr (void)
{
  // efface tout l'écran
  // place le curseur en haut a gauche de l'ecran
  fprintf(stdout, "\033[2J\033[0;0f");
  fflush(stdout);
}

/*======================================================================
**  Nom          : _gotoxy
**  Description  : positionne le curseur
** ---------------------------------------------------------------------
**  Parametres   : [E] [S] [ES]
**      2 [E]    : position ligne
**                 position colonne
**======================================================================*/
void _gotoxy(int x,int y)
{
   fprintf(stdout,"%c[%d;%df",0x1B,x,y);
   fflush(stdout);
}
