#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>


void *monThread(void*);

/*
 * Mes librairies
 */
void *monThread(void* id){
    int var1 = 4;
    int var2 = 2;
    int *i;
    i = (int*)id;

    //Ident
    int _id = pthread_self();

    //Corps du thread
    while(1){
        if(var1 != var2){
            printf("\n thread < %d > var1: %d ; var2: %d",_id,&var1,&var2);
        }
        var1 ++;
        var2 ++;
    }
    pthread_exit(NULL);    
}




/*
 * CODE PRINCIPAL
 */

int main(int argc, char const *argv[])
{
    int _i;
    printf("\n **** InitialisationPool de threads **** \n");
        pthread_t threadPool[4];

    printf("\t Définission priorité / ordonnacement");
        struct sched_param param;
        param.__sched_priority = PTHREAD_EXPLICIT_SCHED;


    printf("Avant la création des thread.\n");
    for(_i=0;_i<4;_i++){
        printf("\n thread %d ..creation ",_i);
        if (pthread_create(&threadPool[_i], &param, monThread, NULL)) {
            perror("pthread_create");
            return EXIT_FAILURE;
        }
    }  

    for(_i=0;_i<4;_i++){
        if (pthread_join(threadPool[_i], NULL)) {
            perror("pthread_join");
            return EXIT_FAILURE;
        }
    }  

    

    printf("Après la création des threads.\n");
        return EXIT_SUCCESS;
    return 0;
}


