package Exercices;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.variables.IntVar;

public class exercice2 {

    public void modelAndSolve(){
        int n = 10;  //nb de taches (A->J)
        int tps_max = 15;
        
        Model model = new Model("Problème Ordonnancement"); 
        IntVar[] vars = new IntVar[n]; //Ensemble des variables
        
        for(int q = 0; q < n; q++){
        	char letter = (char) (65+q);
            vars[q] = model.intVar("t_"+letter, 0, tps_max);  //To pour chaque tache            
        }
        
        //définition des contraintes        
        //mins
        model.arithm(vars[getPos('A')], "<=", vars[getPos('B')], "-", 5 ).post();
        model.arithm(vars[getPos('A')], "<=", vars[getPos('C')], "-", 5 ).post();
        model.arithm(vars[getPos('A')], "<=", vars[getPos('D')], "-", 5 ).post();        
        model.arithm(vars[getPos('C')], "<=", vars[getPos('E')], "-", 3 ).post();
        model.arithm(vars[getPos('C')], "<=", vars[getPos('G')], "-", 3 ).post();
        
        //autres conditions
        model.arithm(vars[getPos('B')], "<=", vars[getPos('E')], "-", 4 ).post();        
        model.arithm(vars[getPos('F')], "<=", vars[getPos('I')], "-", 5 ).post();
        model.arithm(vars[getPos('G')], "<=", vars[getPos('I')], "-", 3 ).post();
        model.arithm(vars[getPos('D')], "<=", vars[getPos('F')], "-", 2 ).post();
        model.arithm(vars[getPos('H')], "<=", vars[getPos('J')], "-", 2 ).post();
        model.arithm(vars[getPos('E')], "<=", vars[getPos('H')], "-", 1 ).post();
        model.arithm(vars[getPos('I')], "<=", vars[getPos('J')], "-", 1 ).post();        

        Solution solution = model.getSolver().findSolution();
        if(solution != null){
        	
            System.out.println(solution.toString());
        }
    }

    public static void main(String[] args) {
        new exercice2().modelAndSolve();
    }
    
    public static int getPos(char letter){
    	int value = (int) letter - (int)'A';
    	return value;
    }
}