package Exercices;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.variables.IntVar;

public class exercice1 {

    public void modelAndSolve(){
        int n = 4;
        Model model = new Model(n + "-xxxx problem"); //queen problem puzzle with 8 columns
        IntVar[] vars = new IntVar[n]; //Ensemble des variables
        
        for(int q = 0; q < n; q++){
            vars[q] = model.intVar("Q_"+q, 1, n);  //Domaine de définition (ie: lignes)
            //model.intVar("X", Integer.MIN_VALUE, Integer.MAX_VALUE)
        }
        
        //définition des contraintes
        for(int i  = 0; i < n-1; i++){
            for(int j = i + 1; j < n; j++){
                model.arithm(vars[i], "!=",vars[j]).post();  //ligne
                model.arithm(vars[i], "!=", vars[j], "-", j - i).post(); //diagonale
                model.arithm(vars[i], "!=", vars[j], "+", j - i).post(); //diagonale
            }
        }
        
        //
        Solution solution = model.getSolver().findSolution();
        if(solution != null){
        	
            System.out.println(solution.toString());
        }
    }

    public static void main(String[] args) {
        new exercice1().modelAndSolve();
    }
}