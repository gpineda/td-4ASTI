package Exercices;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.BoolVar;

public class exercice3 {

    public void modelAndSolve(){
        int n = 5; //nb de nb d'indices
        int m = 5; //nb de transitions
        int k = 5; //nb de ressources
        
        
        Model model = new Model("Problème Sérialisation"); 
        IntVar[] vars = new IntVar[n]; //Ensemble des variables
        //IntVar t = model.intVar("t",1,n);
        
        for(int q = 0; q < n; q++){
            vars[q] = model.intVar("X_"+q, 1, m);  //Ordre d'execution dans le modèle en série recherché       
        }

        //définition des contraintes 
        for(int t  = 1; t < m-1; t++){
            for(int t_ = 1; t_ < m-1; t_++){
            	BoolVar c1= model.arithm(vars[t-1], "!=", vars[t_-1]).reify();
            	int c2= 0;
            	if(t==t_){
            		c2=1;
            	}         	
            	
            	model.arithm(c1,"or",c2).post();
            }
        }
        
        /*
        IntVar t = model.intVar("t",1,n);
        IntVar t_ = model.intVar("t_",1,n);
        
        
        BoolVar c1= model.arithm(vars[t], "!=", vars[t_]).reify();
    	BoolVar c2= model.arithm(t,"=",t_).reify();
    	model.arithm(c1,"or",c2).post();
    	*/
        
        //mins
        /*
        model.arithm(vars[getPos('A')], "<=", vars[getPos('B')], "-", 5 ).post();
        model.arithm(vars[getPos('A')], "<=", vars[getPos('C')], "-", 5 ).post();
        model.arithm(vars[getPos('A')], "<=", vars[getPos('D')], "-", 5 ).post();        
        model.arithm(vars[getPos('C')], "<=", vars[getPos('E')], "-", 3 ).post();
        model.arithm(vars[getPos('C')], "<=", vars[getPos('G')], "-", 3 ).post();
        
        //autres conditions
        model.arithm(vars[getPos('B')], "<=", vars[getPos('E')], "-", 4 ).post();        
        model.arithm(vars[getPos('F')], "<=", vars[getPos('I')], "-", 5 ).post();
        model.arithm(vars[getPos('G')], "<=", vars[getPos('I')], "-", 3 ).post();
        model.arithm(vars[getPos('D')], "<=", vars[getPos('F')], "-", 2 ).post();
        model.arithm(vars[getPos('H')], "<=", vars[getPos('J')], "-", 2 ).post();
        model.arithm(vars[getPos('E')], "<=", vars[getPos('H')], "-", 1 ).post();
        model.arithm(vars[getPos('I')], "<=", vars[getPos('J')], "-", 1 ).post();     
        */   

        Solution solution = model.getSolver().findSolution();
        if(solution != null){
        	
            System.out.println(solution.toString());
        }
    }

    public static void main(String[] args) {
        new exercice3().modelAndSolve();
    }
    
    
}