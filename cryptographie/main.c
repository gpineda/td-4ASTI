#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//user defined library
#include "crypto.h"

/**
*
* Usage: crypto algo sens filein fileout
* 
*/

int main(int argc, char *argv[]){

	

	if(argc==6){

		//main menu here
		
		char* algo    = argv[1];
		char* sens    = argv[2];
		char* filein  = argv[3];
		char* fileout = argv[4];
		char* key     = argv[5];

		/* ------------- XOR -------------- */
		if(!strcmp(algo,"xor")){
			//xor_crypt is the same as xor_decrypt
			xor_crypt(key, filein, fileout);
		}


		/* ------------ Vigenere ----------- */
		if((!strcmp(algo,"vigenere") || !strcmp(algo,"caesar"))){
			//vigenere/caesar
			if(!strcmp(sens,"1")){
				vigenere_crypt(key, filein, fileout);

			}else if(!strcmp(sens,"-1")){
				printf("Decrypting.. \n ");
				vigenere_decrypt(key, filein, fileout);

			}else{
				//error
				printf("[%s] Error .. incorrect type <%s> (1: crypt /-1: decrypt)\n", algo,sens);

			}
			
		}



		/* -------------- DES ------------- */
		if(!strcmp(algo,"des")){
			//vigenere/caesar
			printf(" %s not yet implemented \n",algo);
				
		}

		if(!strcmp(algo,"3des")){
			//vigenere/caesar
			printf(" %s not yet implemented \n",algo);
		}

		if(!strcmp(algo,"aes")){
			//vigenere/caesar
			printf("Waiting %s \n",algo);
		}



	}
			
		
		
		//

	
	else{
		printf("/* Documentation \n * Usage : \n * \n * \n */ \n");
	}
	
	return 1;
}