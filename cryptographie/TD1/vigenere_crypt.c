#include <stdio.h>

char caesar_fct(char a, char b,int mode);
int modulo(int a, int n);

/**
*
* Usage: vigenere key input_file output_file
*
*/

int main(int argc, char *argv[]){
   FILE *fi, *fo;
   char *cp;
   int c;
    
   if(cp = argv[1]){
     if((fi = fopen(argv[2], "rb")) != NULL){
        if( (fo = fopen(argv[3],"wb")) != NULL){
           while ((c=getc(fi))!= EOF){
             if(*cp=='\0') cp = argv[1];
             
	           c = caesar_fct(*(cp++),c,1);
             
             putc(c,fo);
	     
           }
           fclose(fo);
        }
        fclose(fi);        
     }
   }
   return 1;
}


char caesar_fct(char a, char b,int mode){
     // a = clee   ;   b =  mot
     //mode = 1  : crypt  
     //mode = -1 : decrypt

     return 'A' + modulo(((b-'A') + mode * (a-'A')) , 26);
     
}

int modulo(int a, int n){
  return ((a % n) + n) % n;
}
