#include <stdio.h>

char cesar(char a, char b,int mode);

/**
*
* Usage: cesar key input_file output_file
*
*/

int main(int argc, char *argv[]){
   FILE *fi, *fo;
   char *cp;
   int c;
    
   if(cp = argv[1]){
     if((fi = fopen(argv[2], "rb")) != NULL){
        if( (fo = fopen(argv[3],"wb")) != NULL){
           while ((c=getc(fi))!= EOF){
             if(*cp=='\0') cp = argv[1];
             
	     //c = cesar(c,*(cp++),1);
             c = cesar(*(cp++),c,-1);
             putc(c,fo);
	     
           }
           fclose(fo);
        }
        fclose(fi);        
     }
   }
   return 1;
}


char cesar(char a, char b,int mode){
     return ((a-'A') + mode * (b-'A')) % 26 + 'A';

}


