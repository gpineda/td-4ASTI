#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "crypt.h"
#include "encrypt.h"
#include <math.h>



/** Gpineda's defined functions **/
static int modulo(int a, int n){
  return ((a % n) + n) % n;
}

static int vigenere_fct(char a,int ecart){
   // a = clee 
   // b = ecart
   return modulo(((a-'a') + ecart) , 26);
}



static int bloc_padding(char* bloc, char* value, int size){
	// ex bloc  = ABCDE
	//    size  = 8
	//    value = 0
	//    sortie = ABCED000
	
	while(strlen(bloc) < size){
		strcat(bloc, value);
	}	

	return 0;
	
}

static int min(int a, int b){
	if(a <= b){
		return a;
	}else{
		return b;
	}
}



/**
 *  * chiffrement utilisant le ou exclusif
 *   */
void xor_crypt(char * key, char * texte, char* chiffre)
{
	char *kp;  //key_pointer
	int i;     //index_texte

	kp = key;
	for(i=0;i < strlen(texte);i++){
		if(*kp=='\0') kp = key;
		*(chiffre+i) = *(texte+i) ^ *(kp++); //conversion
	}	
}

/**
 *  * d�chiffrement utilisant le ou exclusif
 *   */
void xor_decrypt(char * key, char * chiffre, char* texte)
{
	xor_crypt(key,chiffre,texte);
}

/**
 *  * chiffrement utilisant cesar
 *   */
void cesar_crypt(int decallage, char * texte, char* chiffre)
{	
	int i;	
	for(i=0;i < strlen(texte);i++){		
		char a;
		a = *(texte+i);
		*(chiffre+i) = vigenere_fct(a, decallage)+'a';		
	}	
}

/**
 *  * d�chiffrement utilisant  cesar
 *   */
void cesar_decrypt(int decallage, char * chiffre, char* texte)
{
	int i;	
	for(i=0;i < strlen(chiffre);i++){		
		char a;
		a = *(chiffre+i);
		*(texte+i) = vigenere_fct(a, - decallage)+'a';		
	}
}

/**
 *  * chiffrement utilisant viginere
 *   */
void viginere_crypt(char * key, char * texte, char* chiffre)
{
	char *kp;
	int i;	
	kp = key;
	for(i=0;i < strlen(texte);i++){
		if(*kp=='\0') kp = key;		
		char a = *(texte+i);
		int decallage = *(kp++)-'a';
	
		*(chiffre+i) = vigenere_fct(a, decallage)+'a';		
	}
}

/**
 *  * d�chiffrement utilisant viginere
 *   */
void viginere_decrypt(char * key, char * chiffre, char* texte)
{
	char *kp;
	int i;	

	kp = key;
	for(i=0;i < strlen(chiffre);i++){
		if(*kp=='\0') kp = key;		
		char a = *(chiffre+i);
		int decallage = *(kp++)-'a';
		*(texte+i) = vigenere_fct(a,-decallage)+'a';		
	}
}

/**
 *  * chiffrement utilisant des
 *   */
void des_crypt(char * key, char * texte, char* chiffre, int size)
{
	
	//on d�coupe en blocs de < size > cracteres
	char bloc_in[size];		
	char bloc_out[size];
	char bloc_2[size];

	int i_bloc;
	int nb_blocs = strlen(texte)/size;
	int reste    = modulo(strlen(texte),size);
	printf("%d / %d / %d -- %s \n",size,nb_blocs,reste,texte);
	for(i_bloc=0;i_bloc<=nb_blocs;i_bloc++){
		memset(bloc_in, 0, strlen(bloc_in));
		memset(bloc_out, 0, strlen(bloc_out));
		memset(bloc_2, 0, strlen(bloc_2));
		
		int  nb_to_copy = min(size, strlen(texte)-i_bloc*size);
		
		strncpy(bloc_in, texte+i_bloc*size, nb_to_copy);
		if(nb_to_copy < size){
			//'bourre' le bloc pour atteindre la taille souhait�e
			bloc_padding(bloc_in, " ", size);
		}
		

		//encipher c_bloc
		des_encipher(bloc_in,bloc_out,key);		
		
		//try to decihper
		des_decipher(bloc_out,bloc_2,key);

		strcat(chiffre, bloc_out);
		printf("< %d | %d > chiffre = %s\n",sizeof(bloc_out),strlen(bloc_out), bloc_out );
		printf("< %d | %d > dechiffre = %s\n",sizeof(bloc_2),strlen(bloc_2), bloc_2 );


	}	
	printf("< %d > chiffre = %s\n",strlen(chiffre), chiffre );

}


/**
 *  * d�chiffrement utilisant des
 *   */
void des_decrypt(char * key, char * texte, char* chiffre, int size)
{
	
	int size_coeff = 2;
	size *= size_coeff;
	

	char bloc_in[size];		
	char bloc_out[size];
	char bloc_2[size];

	int i_bloc;
	int nb_blocs = strlen(chiffre)/size;
	int reste    = modulo(strlen(chiffre),size);
	printf("%d / %d / %d -- %s \n",size,nb_blocs,reste,chiffre);
	for(i_bloc=0;i_bloc<=nb_blocs;i_bloc++){
		memset(bloc_in, 0, strlen(bloc_in));
		memset(bloc_out, 0, strlen(bloc_out));
		memset(bloc_2, 0, strlen(bloc_2));
		
		int  nb_to_copy = min(size, strlen(chiffre)-i_bloc*size);
		
		strncpy(bloc_in, chiffre+i_bloc*size, nb_to_copy);
		
		

	
		//try to decihper
		des_decipher(bloc_in,bloc_out,key);

		strcat(texte, bloc_out);
		printf("< %d | %d > chiffre = %s\n",sizeof(bloc_in),strlen(bloc_in), bloc_in );
		printf("< %d | %d > dechiffre = %s\n",sizeof(bloc_out),strlen(bloc_out), bloc_out );


	}	
	printf("< %d > chiffre = %s\n",strlen(chiffre), chiffre );
	printf("< %d > dechiffre = %s\n",strlen(texte), texte );
	

}

/**
 *  * chiffrement utilisant 3des
 *   */
void tripledes_crypt(char * key1, char * key2, char * texte, char* chiffre,int size)
{

}


/**
 *  * d�chiffrement utilisant 3des
 *   */
void tripledes_decrypt(char* key1, char* key2, char* texte, char* chiffre, int size)
{

}


/****************************************************************
 *                                                               *
 *  -------------------------- modexp -------------------------  *
 *                                                               *
 ****************************************************************/

static Huge modexp(Huge a, Huge b, Huge n) {
	
	Huge               y;
	
	/****************************************************************
	 *                                                               *
	 *  Calcule (pow(a, b) % n) avec la m�thode du carr� binaire     *
	 *  et de la multiplication.                                     *
	 *                                                               *
	 ****************************************************************/
	
	y = 1;
	
	while (b != 0) {
		
		/*************************************************************
		 *                                                            *
		 *  Pour chaque 1 de b, on accumule dans y.                   *
		 *                                                            *
		 *************************************************************/
		
		if (b & 1)
			y = (y * a) % n;
		
		/*************************************************************
		 *                                                            *
		 *  �l�vation de a au carr� pour chaque bit de b.             *
		 *                                                            *
		 *************************************************************/
		
		a = (a * a) % n;
		
		/*************************************************************
		 *                                                            *
		 *  On se pr�pare pour le prochain bit de b.                  *
		 *                                                            *
		 *************************************************************/
		
		b = b >> 1;
		
	}
	
	return y;
	
}


/**
 * Transforme une chaine de caract�re en chaine d'entier
 */
void texttoint(char * texte, char* chiffre, int size){
	*chiffre='\0';
	int tmp;
	int i;
	for(i=0;i<size;i++){		
	    // on ajoute 10 pour �viter le probl�me de disparition du 0 devnt les entiers entre 1 et 9 (01 a 09)
		// ceci �vite de d�couper le texte en bloc de taille < n et de les normaliser ensuite
		tmp=(*(texte+i)-'a'+10);
		sprintf(chiffre+strlen(chiffre),"%d%c",tmp,'\0');
	}
}

/**
 * Transforme une chaine d'entier en chaine de caract�re
 */ 
void inttotext(char * texte, char* chiffre){
	*chiffre='\0';
	int tmp=0;
	while((*texte) != '\0'){	
	    // lettre de l'alphabet (0..25 correspond pour nous � 10..35)	
		if(10*tmp+(*(texte)-'0') > 36){
		    // on d�duit donc 10 pour obtenir la bonne lettre dans l'alphabet
			sprintf(chiffre+strlen(chiffre),"%c%c",tmp+'a'-10, '\0');
			tmp=0;
		}
		tmp=10*tmp+(*(texte)-'0');
		texte++;
	}
}

/**
 * Chiffrement RSA
 */
void rsa_crypt(int e, int n, char * texte, char* chiffre, int size)
{
    int tmp;
	Huge buf=0;
	char* pt;
	char* btmp = (char *)malloc(strlen(texte) * sizeof(char)); 
	
	texttoint(texte,btmp,size);
	pt = btmp;
	*chiffre='\0';
	while((*pt) != '\0'){
		tmp=*pt-'0';
		if(10*buf + tmp >= n){
		    // on utilise le $ comme separateur de bloc
			//sprintf(chiffre+strlen(chiffre),"%ld$%c",/* TODO Chiffrement de buf */,'\0');
			sprintf(chiffre+strlen(chiffre),"%ld$%c",modexp(buf,e,n),'\0');

			buf=0;
		}
		buf=10*buf+tmp;
		pt++;
	}
	//sprintf(chiffre+strlen(chiffre),"%ld$%c", int,'\0');
	printf("\n");
}

/**
 * Dechiffrement RSA
 */
void rsa_decrypt(int d, int n, char * texte, char* chiffre)
{
	int tmp;
	char* pt=texte;
	char* tmpc= (char *)malloc(strlen(texte) * sizeof(char)); 
	Huge buf=0;
	
	*tmpc='\0';
	while((*pt) != '\0'){
		// on utilise le $ comme s�parateur de bloc
	    if((*pt) == '$'){
			sprintf(tmpc+strlen(tmpc),"%ld%c", modexp(buf,d,n),'\0');
			buf=0;
		}else{
			tmp=*pt-'0';
			buf=10*buf+tmp;
		}
		pt++;
	}
	//sprintf(tmpc+strlen(tmpc),"%ld%c",/* Dechiffrement de buf*/,'\0');
	
	inttotext(tmpc,chiffre);
}


