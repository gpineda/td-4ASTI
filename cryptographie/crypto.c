#include <stdio.h>
#include "crypto.h"


/******** Mathématiques *******************/

int modulo(int a, int n){
  return ((a % n) + n) % n;
}




/******************** XOR ******************/


void xor_crypt(char* key, char* texte, char* chiffre){
	file_executor_stream(key,texte,chiffre,xor_fct);   
}

void xor_decrypt(char* key, char* chiffre, char* clair){
	file_executor_stream(key,chiffre,clair,xor_fct);   
}


int xor_fct(char a, char b){
	return a ^ b ;
}



/******************** Vigenere ******************/
void vigenere_crypt(char* key, char* texte, char* chiffre){
  file_executor_stream(key,texte,chiffre,vig_fct_crypt);  
}

void vigenere_decrypt(char* key, char* chiffre, char* clair){
  file_executor_stream(key,chiffre,clair,vig_fct_decrypt);  
}







int vigenere_fct(char a,char b, int mode){
     // a = clee   ;   b =  mot
     // mode = 1  : crypt  
     // mode =-1 : decrypt

     return 'A' + modulo(((a-'A') + mode * (b-'A')) , 26);
     
}

int vig_fct_crypt(char a, char b){
  return vigenere_fct(a,b,1);
}

int vig_fct_decrypt(char a, char b){
  return vigenere_fct(a,b,-1);
}













/** Modules de factorisation **/

/*
 * Analyse fichier en flux direct
 * ie: caractere par caractere
 */
int file_executor_stream(char* key, char *f_in, char* f_out, int (*algo)(char, char) ){
	FILE *fi, *fo;
    char *cp;
    int c;

	if(cp = key){
     if((fi = fopen(f_in, "rb")) != NULL){
        if( (fo = fopen(f_out,"wb")) != NULL){
           while ((c=getc(fi))!= EOF){
             if(*cp=='\0') cp = key;
             c = algo(c,*(cp++));
             
             putc(c,fo);
           }
           fclose(fo);
        }else{
          printf("output_file unknown .. \n",f_out);
        }
        fclose(fi);        
     }else{
        printf("input_file unknown .. \n",f_in);
     }
   }
   return 1;
}
