#include <stdio.h>
#include <stdlib.h>

#include <time.h>




typedef unsigned long int Huge;

//Fonctions utilisateur

/*
 *  Random entre a et b
*/
static Huge rand_a_b(Huge a,Huge b){
	return rand()%(b-a) +a;
}


/*
 * Exponentiation modulaire (a^b % n)
 */
static Huge modexp(Huge a,Huge b,Huge n){
	Huge y;
	y=1;
	while(b!=0){
		if(b & 1)
			y = (y*a)%n;
		a = (a * a) % n;
		b = b >> 1;
	}
	return y;
}

/*
 *  GCD Greatest Common Diviser
*/
static Huge gcd( Huge a, Huge b )
{
  Huge c;
  while ( a != 0 ) {
     c = a; a = b%a;  b = c;
  }
  return b;
}


static Huge RSA_init(Huge p, Huge q){
	/*
	 * look for couple of keys from P,Q
	 * (e,n)    (d)
	 */

	Huge e,n,phi,d;
	n = p*q;
	phi = (p-1)*(q-1);

	
	e = 1;
	do{
		e++;
		//e = rand_a_b(2,phi-1)
	} while(gcd(e,phi)!=1);
	

	d = 1;
	do{
		d++;
		//d = rand_a_b(2,phi-1)
	}while((e*d) % phi != 1);


	printf("(e,n) = ( %u , %u) \n",e,n);
	printf("(d,n) = (%u, %u) \n",d,n);

	return e;

}

static void rsa_crypt(int e, int n, char* texte, char* chiffre, int size){
	/*
	 *  input 
	 *		(e,n) pulbic key
	 *            texte   : to cypher
	 *            chiffre : pointeur to encrypted txt
	 *			  size    : 
	 */	

	Huge c,m;
	m = 3333;

	c = modexp(m,e,n);
	printf("crypt : m = %u \n",c);
}


static void rsa_decrypt(int d, int n, char* chiffre, char* clair){
	/*
	 *  input 
	 *		(d,n) private key
	 *            chiffre : encyphered text
	 *            chiffre : pointeur to decrypted text
	 *			  size    : 
	 */	

	Huge c,m;
	c = 3790;

	m = modexp(c,d,n);
	printf("decrypt : m = %u \n",m);

}




/*Usage : 
 *	./rsa P Q  
 *
 */
int main (int argc, char *argv[]){
	
	
	int base_str = 10;


	Huge P = strtol(argv[1],NULL,base_str);
	Huge Q = strtol(argv[2],NULL,base_str);
	

	//RSA_init(strtol(argv[0],10),strtol(argv[1],10));
	
	RSA_init(P,Q);
	rsa_crypt(967,4247,"aaaaa","bbbbb",1);
	rsa_decrypt(2983,4247,"aaaaa","bbbbb");
	
	return 0;

}