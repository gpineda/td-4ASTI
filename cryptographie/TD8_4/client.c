#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <string.h>

/* user libs */
#include "dh_lib.h"

void error(char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    char buffer[256];
    if (argc < 3) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(0);
    }
    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,&serv_addr,sizeof(serv_addr)) < 0) 
        error("ERROR connecting");
    
    printf("Initialisation de la transmission ..");
    
    //TODO DH
    Huge a,b,p,g;

    a = 6;
    b = 15;
    g = 3;
    p = 23;
    
    Huge A = modexp(g,a,p);
    
    //envoi sur Bob
    n = write(sockfd,&p,sizeof(Huge));
    n = write(sockfd,&g,sizeof(Huge));
    n = write(sockfd,&A,sizeof(Huge));

    Huge B,K;
    n = read(sockfd, &B, sizeof(Huge));
    K = modexp(B,a,p);
    

    if(des_setparity(K)){
        //printf("Done .. %li ",K);
        /*
        printf("Please enter the message: ");
        bzero(buffer,256);
        fgets(buffer,255,stdin);

        // ENVOI D UN MESSAGE
        //ecb_crypt(buffer);
        //n = write(sockfd,buffer,strlen(buffer));
        */
    }else{
        //printf("Error on K with parity %li \n",K);
    }


    if (n < 0) 
         error("ERROR writing to socket");
    bzero(buffer,256);
    n = read(sockfd,buffer,255);
    if (n < 0) 
         error("ERROR reading from socket");
    printf("%s\n",buffer);
    return 0;
}
