/*
 * Librairie Diffie-Hellman -- Header --
 *    -> défintion Huge, exp_mod
*/

#ifndef _DH_LIB_

#define _DH_LIB_

typedef unsigned long int Huge;

/*
 * Exponentiation modulaire (a^b % n)
 */

Huge modexp(Huge a,Huge b,Huge n);



#endif
