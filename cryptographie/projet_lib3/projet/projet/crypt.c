#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "crypt.h"
#include "encrypt.h"
#include <math.h>


/** Gpineda's defined functions **/
static int modulo(int a, int n){
  return ((a % n) + n) % n;
}

static int vigenere_fct(char a,int ecart){
   // a = clee 
   // b = ecart
   return modulo(((a-'a') + ecart) , 26);
}

static int min(int a, int b){
	if(a <= b){
		return a;
	}else{
		return b;
	}
}




/**
 *  * chiffrement utilisant le ou exclusif
 *   */
void xor_crypt(char * key, char * texte, char* chiffre, int size)
{
	char *kp;  //key_pointer
	int i;     //index_texte

	kp = key;
	for(i=0;i < size;i++){
		if(*kp=='\0') kp = key;
		*(chiffre+i) = *(texte+i) ^ *(kp++); //conversion
	}	
}

/**
 *  * dechiffrement utilisant le ou exclusif
 *   */
void xor_decrypt(char * key, char * chiffre, char* texte, int size)
{
	xor_crypt(key,chiffre,texte,size);
}

/**
 *  * chiffrement utilisant cesar
 *   */
void cesar_crypt(int decallage, char * texte, char* chiffre)
{
	int i;	
	for(i=0;i < strlen(texte);i++){		
		char a;
		a = *(texte+i);
		*(chiffre+i) = vigenere_fct(a, decallage)+'a';		
	}	
}

/**
 *  * dechiffrement utilisant  cesar
 *   */
void cesar_decrypt(int decallage, char * chiffre, char* texte)
{
	int i;	
	for(i=0;i < strlen(chiffre);i++){		
		char a;
		a = *(chiffre+i);
		*(texte+i) = vigenere_fct(a, - decallage)+'a';		
	}
}

/**
 *  * chiffrement utilisant viginere
 *   */
void viginere_crypt(char * key, char * texte, char* chiffre)
{
	char *kp;
	int i;	
	kp = key;
	for(i=0;i < strlen(texte);i++){
		if(*kp=='\0') kp = key;		
		char a = *(texte+i);
		int decallage = *(kp++)-'a';
	
		*(chiffre+i) = vigenere_fct(a, decallage)+'a';		
	}
}

/**
 *  * dechiffrement utilisant viginere
 *   */
void viginere_decrypt(char * key, char * chiffre, char* texte)
{
	char *kp;
	int i;	

	kp = key;
	for(i=0;i < strlen(chiffre);i++){
		if(*kp=='\0') kp = key;		
		char a = *(chiffre+i);
		int decallage = *(kp++)-'a';
		*(texte+i) = vigenere_fct(a,-decallage)+'a';		
	}
}

/**
 *  * chiffrement utilisant des ECB
 *   */
void des_ecb_crypt(char * key, char * texte, char* chiffre, int size)
{
    
}


/**
 *  * dechiffrement utilisant des ECB
 *   */
void des_ecb_decrypt(char * key, char * chiffre, char* texte, int size)
{
    
}

/**
 *  * chiffrement utilisant 3des ECB
 *   */
void tripledes_ecb_crypt(char * key1, char * key2, char * texte, char* chiffre,int size)
{
    
}


/**
 *  * dechiffrement utilisant 3des ECB
 *   */
void tripledes_ecb_decrypt(char* key1, char* key2, char* chiffre, char* texte, int size)
{
    
}

/**
 *  * chiffrement utilisant des CBC
 *   */
void des_cbc_crypt(char * key, char * texte, char* chiffre, int size)
{

}


/**
 *  * dechiffrement utilisant des CBC
 *   */
void des_cbc_decrypt(char * key, char * chiffre, char* texte, int size)
{

}

/**
 *  * chiffrement utilisant 3des CBC
 *   */
void tripledes_cbc_crypt(char * key1, char * key2, char * texte, char* chiffre,int size)
{

}


/**
 *  * dechiffrement utilisant 3des CBC
 *   */
void tripledes_cbc_decrypt(char* key1, char* key2, char* texte, char* chiffre, int size)
{

}


/****************************************************************
 *                                                               *
 *  -------------------------- modexp -------------------------  *
 *                                                               *
 ****************************************************************/

static Huge modexp(Huge a, Huge b, Huge n) {
	
	Huge               y;
	
	/****************************************************************
	 *                                                               *
	 *  Calcule (pow(a, b) % n) avec la methode du carre binaire     *
	 *  et de la multiplication.                                     *
	 *                                                               *
	 ****************************************************************/
	
	y = 1;
	
	while (b != 0) {
		
		/*************************************************************
		 *                                                            *
		 *  Pour chaque 1 de b, on accumule dans y.                   *
		 *                                                            *
		 *************************************************************/
		
		if (b & 1)
			y = (y * a) % n;
		
		/*************************************************************
		 *                                                            *
		 *  �levation de a au carre pour chaque bit de b.             *
		 *                                                            *
		 *************************************************************/
		
		a = (a * a) % n;
		
		/*************************************************************
		 *                                                            *
		 *  On se prepare pour le prochain bit de b.                  *
		 *                                                            *
		 *************************************************************/
		
		b = b >> 1;
		
	}
	
	return y;
	
}


/**
 * Transforme une chaine de caractere en chaine d'entier
 */
void texttoint(char * texte, char* chiffre, int size){
	*chiffre='\0';
	int tmp;
	int i;
	for(i=0;i<size;i++){		
	    // on ajoute 10 pour �viter le probleme de disparition du 0 devnt les entiers entre 1 et 9 (01 a 09)
		// ceci �vite de d�couper le texte en bloc de taille < n et de les normaliser ensuite
		tmp=(*(texte+i)-'a'+10);
		sprintf(chiffre+strlen(chiffre),"%d%c",tmp,'\0');
	}
}

/**
 * Transforme une chaine d'entier en chaine de caractere
 */ 
void inttotext(char * texte, char* chiffre){
	*chiffre='\0';
	int tmp=0;
	while((*texte) != '\0'){	
	    // lettre de l'alphabet (0..25 correspond pour nous � 10..35)	
		if(10*tmp+(*(texte)-'0') > 36){
		    // on d�duit donc 10 pour obtenir la bonne lettre dans l'alphabet
			sprintf(chiffre+strlen(chiffre),"%c%c",tmp+'a'-10, '\0');
			tmp=0;
		}
		tmp=10*tmp+(*(texte)-'0');
		texte++;
	}
}

/**
 * Chiffrement RSA
 */
void rsa_crypt(int e, int n, char * texte, char* chiffre, int size)
{
    int tmp;
	Huge buf=0;
	char* pt;
	char* btmp = (char *)malloc(strlen(texte) * sizeof(char)); 
	
	texttoint(texte,btmp,size);
	pt = btmp;
	*chiffre='\0';
	while((*pt) != '\0'){
		tmp=*pt-'0';
		if(10*buf + tmp >= n){
		    // on utilise le $ comme s�parateur de bloc
			//sprintf(chiffre+strlen(chiffre),"%ld$%c",/* TODO Chiffrement de buf */,'\0');
			buf=0;
		}
		buf=10*buf+tmp;
		pt++;
	}
	//sprintf(chiffre+strlen(chiffre),"%ld$%c", /* TODO Chiffrement de Buf */,'\0');
	printf("\n");
}

/**
 * D�chiffrement RSA
 */
void rsa_decrypt(int d, int n, char * texte, char* chiffre)
{
	int tmp;
	char* pt=texte;
	char* tmpc= (char *)malloc(strlen(texte) * sizeof(char)); 
	Huge buf=0;
	
	*tmpc='\0';
	while((*pt) != '\0'){
		// on utilise le $ comme s�parateur de bloc
	    if((*pt) == '$'){
			//sprintf(tmpc+strlen(tmpc),"%ld%c", /* Dechiffrement de buf */,'\0');
			buf=0;
		}else{
			tmp=*pt-'0';
			buf=10*buf+tmp;
		}
		pt++;
	}
	//sprintf(tmpc+strlen(tmpc),"%ld%c",/* Dechiffrement de buf*/,'\0');
	
	inttotext(tmpc,chiffre);
}

/**
 * Hashage md5
 * Retourne le hash dans une chaine de carat�re hash au format hexad�cimal
 */
void md5(char *texte, char *hash){
    
    int i;
	unsigned char digest [16];

	md5 (str, strlen ((char *)str), digest);

	/* Display the md5 sum */
	for (i=0; i<16; i++)
		printf ("%02x", digest [i]);
}
