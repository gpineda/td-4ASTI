/**
 *  * chiffrement utilisant le ou exclusif
 *   */
void xor_crypt(char* key, char* texte, char* chiffre,int taille);

/**
 *  * dechiffrement utilisant le ou exclusif
 *   */
void xor_decrypt(char* key, char* chiffre, char* clair,int taille);

/**
 *  * chiffrement utilisant cesar
 *   */
void cesar_crypt(int decallage, char* texte, char* chiffre);

/**
 *  * dechiffrement utilisant  cesar
 *   */
void cesar_decrypt(int decallage, char* chiffre, char* clair);

/**
 *  * chiffrement utilisant viginere
 *   */
void viginere_crypt(char* key, char* texte, char* chiffre);

/**
 *  * dechiffrement utilisant viginere
 *   */
void viginere_decrypt(char* key, char* chiffre, char* clair);


/**
 *  * chiffrement utilisant des ECB
 *   */
void des_ecb_crypt(char * key, char * texte, char* chiffre, int size);


/**
 *  * dechiffrement utilisant des ECB
 *   */
void des_ecb_decrypt(char * key, char * chiffre, char* texte, int size);

/**
 *  * chiffrement utilisant 3des ECB
 *   */
void tripledes_ecb_crypt(char * key1, char * key2, char * texte, char* chiffre,int size);


/**
 *  * dechiffrement utilisant 3des ECB
 *   */
void tripledes_ecb_decrypt(char* key1, char* key2, char* chiffre, char* texte, int size);

/**
 *  * chiffrement utilisant des CBC
 *   */
void des_cbc_crypt(char * key, char * texte, char* chiffre, int size);


/**
 *  * dechiffrement utilisant des CBC
 *   */
void des_cbc_decrypt(char * key, char * chiffre, char* texte, int size);

/**
 *  * chiffrement utilisant 3des CBC
 *   */
void tripledes_cbc_crypt(char * key1, char * key2, char * texte, char* chiffre,int size);


/**
 *  * dechiffrement utilisant 3des CBC
 *   */
void tripledes_cbc_decrypt(char* key1, char* key2, char* chiffre, char* texte, int size);


/**
 * Chiffrement RSA
 */
void rsa_crypt(int e, int n, char* texte, char* chiffre, int size);

/**
 * Dechiffrement RSA
 */
void rsa_decrypt(int d, int n, char* chiffre, char* clair);


/**
 * Hashage md5
 * Retourne le hash dans une chaine de caratère hash au format hexadécimal
 */
void md5(char *texte, char *hash);


