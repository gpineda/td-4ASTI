/*
   crypto.h
   ------

   Contient les prototypes des fonctions de crypto.c
*/


#ifndef H_CRYPTO
#define H_CRYPTO


/**
* XOR
**/ 
void xor_crypt(char* key, char* texte, char* chiffre);

void xor_decrypt(char* key, char* chiffre, char* clair);

int xor_fct(char, char);

/**
* CAESAR
**/ 
void cesar_crypt(char* key, char* texte, char* chiffre);

void cesar_decrypt(char* key, char* chiffre, char* clair);


/**
* VIGENERE
**/ 
void vigenere_crypt(char* key, char* texte, char* chiffre);

void vigenere_decrypt(char* key, char* chiffre, char* clair);

int vigenere_fct(char a,char b, int mode);
int vig_fct_crypt(char a, char b);
int vig_fct_decrypt(char a, char b);


/**
 * DES
 */





/* Modules pour factorisation de code */
int file_executor_stream(char* key, char* f_in, char* f_out, int (*algo)(char, char));

/* fonctions mathématiques */
int modulo(int a, int n);
#endif
