#Script generation du compteur

mat_transition = [1,2,5,10]

l_states = [0]

for i in range(17):
   #print("\t"+str(i)+" = _"+str(i)+";")
   c_state     = min(l_states)
   l_states    = []
   for transition in mat_transition:
      n_state     = c_state + transition
      if(n_state <= 17):
         l_states.append(n_state)
         print("\t"+str(c_state) + " -> " + str(n_state) + "[inc"+str(transition)+"] ;")	

