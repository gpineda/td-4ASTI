package main;

import acteur.Lecteur;
import acteur.Redacteur;
import ressource.Ressource;

public class Main {

	public static void main(String[] args) {
		int m = 20;
		
		//Test_initialisation();
		//Question_1_a();
		
		Question_1_b(m,2*m);
		
	}
	
	
	
	public static void Test_initialisation(){
		Ressource r = new Ressource();
		Redacteur r1 = new Redacteur("Redacteur 1 ",r);
		
		r1.Hello();
		r1.start();
		r.Display();
	}
	
	public static void Question_1_a(){
		System.out.println("Question 1a : ");
		Question_1_b(3,5);
		
		
		
		
	}
	
	public static void Question_1_b(int nb_redacteurs,int nb_lecteurs){
Ressource ressoure = new Ressource();
		
		Redacteur[] redacteurs = new Redacteur[nb_redacteurs];
		Lecteur[] lecteurs = new Lecteur[nb_lecteurs];
		
		//On crée nb_lecteurs lecteurs :
		for(int i = 0;i<redacteurs.length;i++){
			redacteurs[i] = new Redacteur("Redacteur_"+i,ressoure);
			redacteurs[i].Hello();
			
		}
		
		//nb_redacteurs rédacteurs :
		for(int i = 0;i<lecteurs.length;i++){
			lecteurs[i] = new Lecteur("Lecteur_"+i,ressoure);
			lecteurs[i].Hello();
		}
		
		
		//On démare les Redacteurs
		for(Redacteur ri : redacteurs)
			ri.start();
		
		//On démare les Lecteurs
		for(Lecteur li : lecteurs)
			li.start();
	}

}
