package acteur;


import ressource.Ressource;

public class Lecteur extends Thread{
	private String name = "default reader";						//Nom du lecteur
	private Ressource r;										//Ressource sur laquelle lire
	
	public Lecteur(String _name,Ressource _r){
		this.name = _name;						
		this.r = _r;											//Affectation de la ressource
	}
	
	public void run(){			
		for(int i=0;i<5;i++) 			//5 itérations du lecteur
			this.loop();					//Lecture
	}
	
	public void loop(){
		String lname = "not found";
		synchronized(this.r){
			lname = this.r.getLastName();						//Obtention du nom du dernier redacteur
		}
		System.out.println(this.name + " is reading .. : "+lname);
		try {
			sleep(5*1000);										//REPOS
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void Hello(){
		System.out.println(this.name);							//Méthode DEBUG qui retourne nom du lecteur
	}
}
