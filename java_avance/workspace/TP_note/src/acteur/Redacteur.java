package acteur;

import ressource.Ressource;

public class Redacteur extends Thread {
	private String name;			//Son nom
	private Ressource r;			//Ressource sur laquelle écrire
	
	public Redacteur(String _name, Ressource _r){
		this.name = _name; 						//On nomme notre rédacteur
		this.r = _r;							//Affectation de la ressource
	}
	
	public void Hello(){
		System.out.println(this.name);			//DEBUG / UI : affiche son nom
	}
	
	public void run(){							//Méthode run liée au Thread
		
		for(int i=0;i<10;i++)					//10 itérations
			this.loop();							//Ecriture du nom
		
	}
	
	public void loop(){
		synchronized(this.r){					//Prend la ressource
			this.r.addName(this.name);  		//Modifie la ressource
			
		}
		try {
			sleep(3*1000);						//Attendre 3s
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//this.r.Display();						//DEBUG: affiche la ressource
	}
	

}
