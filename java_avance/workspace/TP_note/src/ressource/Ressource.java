package ressource;

import java.util.Vector;

public class Ressource {
	private Vector<String> chaine;					//Vecteur à la place de liste (gère mieux multithread)
	private int nb_lectures = 0;

	
	public Ressource(){
		this.chaine = new Vector<String>();			//Initilisation de la ressource
	};
	
	public Vector<String> getChaine(){
		return this.chaine;							//Retourne le vecteur en entier
	}
	
	public String getLastName(){
		this.nb_lectures++;
		this.nb_lectures--;
		return this.chaine.lastElement();			//Retourne le dernier nom enregistré
		
	}
	
	public Vector<String> setChaine(Vector<String> _chaine){
		return this.chaine = _chaine;				//Seter sur la chaine
	}
	
	public void addName(String name){
		this.chaine.add(name);						//Ajout nom en fin de chaine
	}
	
	
	
	public void Display(){
		System.out.println(this.chaine);			//Affiche la chaine complète
	}
}
