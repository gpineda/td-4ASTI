package tools;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Introspection {
	
	
	public static void analyze(Class myclass){
		Field[] fields = myclass.getFields();
		Method[] methods = myclass.getMethods();
		
		System.out.println(myclass+" ... "+fields.length+" .. "+methods.length);
		
		
		for(Field tmp_field : fields)
			System.out.println(tmp_field.getName());
		
		for(Method tmp_methods : methods)
			System.out.println("<"+tmp_methods.isVarArgs() +"> "+tmp_methods);
			//analyze(myclass,tmp_methods.getName());
	}
	
	public static void analyze(Class myclass,String method_name, Class[] args){
		//Retourne les prototypes
				try {
					//Method gmias = myclass.getMethod(method_name);
					Method gmias = myclass.getMethod(method_name,args);
					System.out.println("instrospection .. : "+gmias.getName());						
					System.out.println(" type de retour .:  "+gmias.getReturnType());					
					
					for(Class cls_tmp : gmias.getParameterTypes())
						System.out.println(" Paramètres .:  "+cls_tmp.getName());					
					
					System.out.println("prototypage .. : "+gmias);
					
				} catch (NoSuchMethodException e) {
					//System.err.println("NoSuchMethodException");
					e.printStackTrace();
				} catch (SecurityException e) {
					//System.err.println("SecurityException");
					e.printStackTrace();
				}
	}
	
	
	public static boolean hasMethod(Class cls,String m_name){
		Method[] methods = cls.getMethods();
		for (Method m : methods) {
		  if (m.getName().equals(m_name)) {
		    return true;
		  }
		}
		return false;
	}
	
}
