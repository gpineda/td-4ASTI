package main;



import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.Scanner;

import tools.Introspection;
import usine.ObjectProvider;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ObjectProvider objp = new ObjectProvider();
		System.out.println(objp);
		
		String FILENAME = "/tmp/myfile_";
		
		//Introspection Q3
		Class cls = objp.getClass();
		Introspection.analyze(cls);
		
		//Q4
		Introspection.analyze(cls,"giveMeIntegersAndStrings",null);
		Class[] parametres = new Class[1];
		
		
		parametres[0] = Object.class;
		Introspection.analyze(cls,"storeSimilarObjects",parametres);
		
		
		//Q5
		Collection<Object> clobjs = objp.giveMeIntegersAndStrings();
		
		/*
		for(Object tmpobj : clobjs){
			System.out.println(tmpobj);
			if(tmpobj instanceof String){
				System.out.println("\t \t String");
			}else if(tmpobj instanceof Integer){
				System.out.println("\t \t Integer");
			}
			
		}
		*/
		System.out.println(clobjs);
		
		//Q6
		for(Object tmpobj : clobjs){
			System.out.println("-------------------------------------------------------");
			Class cls_tmp = tmpobj.getClass();
			Constructor c[] = cls_tmp.getConstructors();
			for(Constructor tmp_constructor : c){
				System.out.println(tmp_constructor);
			}
			
			//a la méthode byteValue ?
			Introspection.analyze(cls_tmp);
			
			//Q7
			if(Introspection.hasMethod(cls_tmp,"byteValue")){
				System.out.println("J'ai");
			}
		}
		
		//Ecriture dans un fichier ..
	    
	    try {
			FileOutputStream fos = new FileOutputStream(FILENAME+".bin");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
	    	BufferedWriter writer = new BufferedWriter(new FileWriter(FILENAME));
	    	for(Object tmpobj : clobjs){
	    		System.out.println("tmpojbs : "+tmpobj);
	    		Object(tmpobj);
	    		writer.write(tmpobj.toString());
	    		writer.newLine();
	    	}
			
			writer.close();
			oos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	     
	    
	    //Lecture et Tokenize
	    
	    
	    BufferedReader br = null;
		FileReader fr = null;
		

			
			try {
				fr = new FileReader(FILENAME);
				br = new BufferedReader(fr);
				String sCurrentLine;
				while ((sCurrentLine = br.readLine()) != null) {
					System.out.println(sCurrentLine);
					//Parse the line with scanner
					Scanner myScan = new Scanner(sCurrentLine);
					myScan.useDelimiter(" ");
					while(myScan.hasNext()){
						System.out.println("\t "+myScan.next());
					}
					
						
				}
			} catch (IOException e) {
	
				e.printStackTrace();
	
			} finally {
				try {
					if (br != null)
						br.close();
					if (fr != null)
						fr.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}	
		
		
		
		
		
		//Serialize in file
		
		
	}

}
