package transport;

public class Camion implements Véhicule {
	private double speed = 0;
	private String color = "blue";
	
	public Camion(double d, String color){
		this.speed = d;
		this.color = color;
		System.out.println("new Camion :");
		this.getStats();
	}
	
	public void stop(){
		System.out.println("STOOOOOOP");
		this.setSpeed(0);
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	@Override
	public void getStats() {
		System.out.println("Current Statistics");
		// TODO Auto-generated method stub
		System.out.println("Speed : <"+this.speed+">");
		System.out.println("Color : <"+this.color+">");

	}
}
