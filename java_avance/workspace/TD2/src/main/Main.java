package main;

import ressource.Ressource;
import thread.MyThread;

public class Main {
	
	public static void main(String args[]){
		
		//premiers threads
			//main_ex1("myThread",10);
			//main_ex2(2,1);
		
		//Deadlock
			//main_ex4(2,1);
			
		//les arguments ici correspondent au nombre de boucles pour thread 1 & 2
			//main_ex8(1,1);
			main_ex8(100,100); 

			
			
	
	}
	
	public static void main_ex1(String name, int n) {
		MyThread t1 = new MyThread(n,name);
		t1.start();
	}	
	
	public static void main_ex2(int n1, int n2) {
		MyThread t1 = new MyThread(n1,"_1");
		MyThread t2 = new MyThread(n2,"_2");
		
		t1.start();
		t2.start();
	}
	
	public static void main_ex4(int n1, int n2) {
		Ressource r1 = new Ressource("_r1_");
		Ressource r2 = new Ressource("_r2_");

		MyThread t1 = new MyThread(n1,"_1",r1,r2);
		MyThread t2 = new MyThread(n2,"_2",r1,r2);
		
		
		t1.start();
		t2.start();
	}
	
	public static void main_ex8(int n1, int n2) {
		Ressource r1 = new Ressource("_r1_");
		Ressource r2 = new Ressource("_r2_");

		MyThread t1 = new MyThread(n1,"0_1",r1,r2);
		MyThread t2 = new MyThread(n2,"0_2",r1,r2);
		
		
		t1.start();
		t2.start();
	}
	
	
	
	

}
