package thread;

import ressource.Ressource;

public class MyThread extends Thread {
	private String name;
	private int n;
	
	private Ressource r1;
	private Ressource r2;
	
	//Constructeur par défaut
	public MyThread(int n,String nom){
		this(n,nom,new Ressource("r1"), new Ressource("r2"));
	}
	
	public MyThread(int n,String nom, Ressource r1, Ressource r2){
		//n: delay  ;  nom: nom thread
		this.name = nom;
		this.n = n;
		this.r1 = r1;
		this.r2 = r2;
	}
	
	
	//
	public void run(){
		
		//this.run1();
		//this.run2();
		if(this.name=="_1" || this.name=="0_1"){
			int sleep1 = 3000;
			if(this.name=="0_1"){
				sleep1=0;
			}
			
			for(int i=0;i<this.n;i++){
				System.out.println("<"+this.name+"> Boucle .. :"+i);
				run2(sleep1);
			}
			
		}else if(this.name=="_2" || this.name=="0_2"){
			int sleep1 = 1000;
			int sleep2 = 3000;
			
			if(this.name=="0_2"){
				sleep1=0;
				sleep2=0;
			}
			
			for(int i=0;i<this.n;i++){
				System.out.println("<"+this.name+"> Boucle .. :"+i);
				run3(sleep1,sleep2);
			}
			
			
		}else{
			run1();
		}
		System.out.println("Finished <"+this.name+">");
	}
	
	public void run2(int sleep1){
		synchronized(this.r1){
			System.out.println("< "+ this.name +" > prend un moniteur sur : "+this.r1.getName());			
			try {
				sleep(sleep1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			synchronized(this.r2){
				System.out.println("< "+ this.name +" > prend un moniteur sur : "+this.r2.getName());
				
				System.out.println("Je suis < "+ this.name +" > et j'ai les deux ressrouces");
			}			
		}		
		
	}
	
	public void run3(int sleep1,int sleep2){
		try {
			sleep(sleep1);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		synchronized(this.r2){
			System.out.println("< "+ this.name +" > prend un moniteur sur : "+this.r2.getName());
			try {
				sleep(sleep2);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			synchronized(this.r1){
				System.out.println("< "+ this.name +" > prend un moniteur sur : "+this.r1.getName());
				System.out.println("Je suis < "+ this.name +" > et j'ai les deux ressrouces");
			}
			
			
		}
		
		
		
		
		
	}
	
	public void run1(){
		System.out.println("Run");
		System.out.println(this.name);
		System.out.println(this.n);
		System.out.println("==============");
		
		//boucle 20fois
		for(int i=0;i<20;i++){
			System.out.println(this.name);
			
			//pause n-secondes
			try {
				sleep(n*1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
