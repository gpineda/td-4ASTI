# Java avancé TD1
------------------

## 1-Jar
#### Exercice1
```java
    package transport;
    public interface Véhicule {
    	public void getStats(); //return statistics
    	public void stop(); // set speed to null 
    }
```
```java
    package transport;
    public class Camion implements Véhicule {
    	private double speed = 0;
    	private String color = "blue";
    	public Camion(double d, String color){
    		this.speed = d;
    		this.color = color;
    		System.out.println("new Camion :");
    		this.getStats();
    	}
    	public void stop(){
    		System.out.println("STOOOOOOP");
    		this.setSpeed(0);
    	}
    	public double getSpeed() {
    		return speed;
    	}
    	public void setSpeed(double speed) {
    		this.speed = speed;
    	}
    	@Override
    	public void getStats() {
    		System.out.println("Current Statistics");
    		// TODO Auto-generated method stub
    		System.out.println("Speed : <"+this.speed+">");
    		System.out.println("Color : <"+this.color+">");
    	}
    }

``` 
#### Exercice3

```java
package main;
import java.util.Vector;
import transport.Camion;
public class Main {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Vector<Camion> Cs = new Vector<Camion>();
		Cs.add(new Camion(10.0,"blue"));
		Cs.add(new Camion(20.0,"green"));
		Cs.add(new Camion(30.0,"red"));
	}
}
```

### Exercice4/5
```bash
    gpineda@SA206-01A:~/Documents/4ASTI_git/td-4ASTI/java_avance$ java -jar TD1_bis.jar 
    > new Camion :
    > Current Statistics
    > Speed : <10.0>
    > Color : <blue>
    > new Camion :
    > Current Statistics
    > Speed : <20.0>
    > Color : <green>
    > new Camion :
    > Current Statistics
    > Speed : <30.0>
    > Color : <red>
```
## 2-Décompilation

### Exercice6

avec javap : Java Class File Disassembler ([site officiel](https://docs.oracle.com/javase/7/docs/technotes/tools/windows/javap.html))
```bash
    gpineda@SA206-01A:~/Documents/4ASTI_git/td-4ASTI/java_avance$ javap TD1_bis/main/Main.class 
    Compiled from "Main.java"
    public class main.Main {
      public main.Main();
      public static void main(java.lang.String[]);
    }
```

```bash
    gpineda@SA206-01A:~/Documents/4ASTI_git/td-4ASTI/java_avance$ javap -c TD1_bis/main/Main.class 
    Compiled from "Main.java"
    public class main.Main {
      public main.Main();
        Code:
           0: aload_0
           1: invokespecial #8                  // Method java/lang/Object."<init>":()V
           4: return
    
      public static void main(java.lang.String[]);
        Code:
           0: new           #16                 // class java/util/Vector
           3: dup
           4: invokespecial #18                 // Method java/util/Vector."<init>":()V
           7: astore_1
           8: aload_1
           9: new           #19                 // class transport/Camion
          12: dup
          13: ldc2_w        #21                 // double 10.0d
          16: ldc           #23                 // String blue
          18: invokespecial #25                 // Method transport/Camion."<init>":(DLjava/lang/String;)V
          21: invokevirtual #28                 // Method java/util/Vector.add:(Ljava/lang/Object;)Z
          24: pop
          25: aload_1
          26: new           #19                 // class transport/Camion
          29: dup
          30: ldc2_w        #32                 // double 20.0d
          33: ldc           #34                 // String green
          35: invokespecial #25                 // Method transport/Camion."<init>":(DLjava/lang/String;)V
          38: invokevirtual #28                 // Method java/util/Vector.add:(Ljava/lang/Object;)Z
          41: pop
          42: aload_1
          43: new           #19                 // class transport/Camion
          46: dup
          47: ldc2_w        #36                 // double 30.0d
          50: ldc           #38                 // String red
          52: invokespecial #25                 // Method transport/Camion."<init>":(DLjava/lang/String;)V
          55: invokevirtual #28                 // Method java/util/Vector.add:(Ljava/lang/Object;)Z
          58: pop
          59: return
    }
```

## 3-Garbage collector

### Exercice7
```java
    package main;
    import java.util.Vector;
    import transport.Camion;
    public class Main {
    	public static void main(String[] args) {
    		// TODO Auto-generated method stub
    		Vector<Camion> Cs = new Vector<Camion>();
    		int max = 999999;
    		int i;
    		for(i=1;i<max;i++){
    			//all %5 iterations : delete
    			if(i%5==0){
    				//delete
    				int pos = i % Cs.size();
    				Cs.remove(pos);
    			}else{
    				//create
    				Cs.add(new Camion(10.0,"blue"));
    			}
    		}
    		System.out.println("nb_iter : "+i);
    		System.out.println("nb_camions :"+Cs.size());
    	}
    }
```

```bash
    ........
    nb_iter : 999999
    nb_camions :600000
```
Avec Window > Préférences > showHeap status (On)
On voit qu'au cours du temps, avec une itération : 9999999.. on remarque que l'allocation diminue après dépassement de 400Mb à 125 .. c'est le garbage collector

Avec la librairie Runtime:

 .. plus de réseau pour les recherches .. idée : totalMemory()
